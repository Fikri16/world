import 'package:flutter/material.dart';
import 'package:world/pages/not_found.dart';
import 'package:world/pages/products.dart';
import 'package:world/pages/products_admin.dart';
import './pages/product.dart';


void main() {
  // debugPaintSizeEnabled = true;
  // debugPaintBaselinesEnabled = true;
  // debugPaintPointersEnabled = true;
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Map<String, String>> _products = [];

  void _addProduct(Map<String, String> product) {
    setState(() {
      _products.add(product);
      print(_products);
    });
  }

  void _deleteProduct(int index){
    setState(() {
     _products.removeAt(index);
    });
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/admin': (BuildContext context) => ProductsAdminPage(),
        '/': (BuildContext context) => ProductsPage(_products,_addProduct, _deleteProduct)
      },
      onGenerateRoute: (RouteSettings settings) {
        final List<String> pathElements = settings.name.split('/');

        if(pathElements[0]!=''){
          return null;
        }

        if (pathElements[1]=='product'){
          final int index= int.parse(pathElements[2]);
          return MaterialPageRoute<bool>(
            builder: (BuildContext context) => ProductPage(
                title: _products[index]['title'],
                imageUrl: _products[index]['imageUrl']
              ));
        }
        return null;
      },
      onUnknownRoute: (RouteSettings settings){
        return MaterialPageRoute(
          builder: (BuildContext context) => NotFoundPage(),
        );
      },
      theme: ThemeData(primarySwatch: Colors.deepOrange),
      title: 'Hello World',
      // home: AuthPage(),
      // debugShowCheckedModeBanner: false,
    );
  }
}
