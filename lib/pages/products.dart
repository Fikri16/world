import 'package:flutter/material.dart';
// import 'package:world/pages/products_admin.dart';
import 'package:world/product_manager.dart';
// import '../product_manager.dart';

class ProductsPage extends StatelessWidget {
  final List<Map<String, String>> products;
  final Function addProduct;
  final Function deleteProduct;

  ProductsPage(this.products,this.addProduct ,this.deleteProduct);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
          child: Column(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
            title: Text('Menu'),
          ),
          ListTile(
            title: Text('Manage Products'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/admin');
            },
            leading: Icon(Icons.add),
          )
        ],
      )),
      appBar: AppBar(
        title: Text("Aplikasiku"),
      ),
      // body: ProductsAdminPage(),
      body: ProductManager(products, addProduct, deleteProduct),
    );
  }
}
