import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
// import 'package:world/pages/product.dart';
// import './pages/product.dart';

class Products extends StatelessWidget {
  final List<Map<String, String>> products;
  final Function deleteProduct;
  Products(this.products, {this.deleteProduct}) {
    print('[Product Widget] Constructor');
  }

  Widget _buildProductItem(BuildContext context, int index) {
    return Card(
      child: Column(
        children: <Widget>[
          Image.asset(products[index]['imageUrl']),
          Text(products[index]['title']),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                  child: Text('Details'),
                  onPressed: () => Navigator.pushNamed<bool>(
                              context, '/product/' + index.toString())
                          .then((bool value) {
                        if (value) {
                          deleteProduct(index);
                        }
                      })),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildProductList() {
    Widget _productCard;
    if (products.length > 0) {
      _productCard = ListView.builder(
        itemBuilder: _buildProductItem,
        itemCount: products.length,
      );
    } else
      _productCard = Center(
        child: Text('No item Found. Please Add Product First'),
      );
    return _productCard;
  }

  @override
  Widget build(BuildContext context) {
    print('[Product Widget] build()');
    return _buildProductList();
  }
}
